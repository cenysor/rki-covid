#! /usr/bin/python3

# Robert Koch-Institut COVID-19 API

import requests, bs4, datetime, shelve, sys, re

class Bundesland:

    def __init__(self, data):
        self.name = data[0].replace('\xad', '')
        self.infected = int(data[1].replace('.', '').strip('*'))
        self.difference = int(data[2].replace('.', '').strip('*'))
        self.per_capita = int(data[3].replace('.', '').strip('*'))
        if data[4] == '':
            self.deaths = 0
        else:
            self.deaths = int(data[4].replace('.', '').strip('*'))

    def calculate(self):
        self.per_person = self.per_capita / 100000
        self.inhabitants = int(self.infected / self.per_person)
        self.diff_fract = 1 - ((self.infected - self.difference) / self.infected)
        self.death_rate = self.deaths / self.infected

    def average(self, sumlist):
        diffsum = 0
        for i in sumlist:
            diffsum += i.diff_fract
        self.av_diff_fract = diffsum / len(sumlist)

    def compare(self, yesterday):
        self.acceleration = self.diff_fract - yesterday.diff_fract
        self.death_diff = self.deaths - yesterday.deaths
        if self.deaths == 0:
            self.death_diff_fract = 0
        else:
            self.death_diff_fract = 1 - ((self.deaths - self.death_diff) / self.deaths)
        self.death_acc = self.death_rate - yesterday.death_rate

def display():
    print('\n------------------------------------------------------------------')
    for bundesland in bundesland_list:
        print('\n' + bundesland.name + ':')
        print('Einwohner:', bundesland.inhabitants)
        print('Infektionen:', bundesland.infected)
        print('Differenz zum Vortag:', bundesland.difference)
        plus = ''
        if bundesland.acceleration > 0:
            plus = '+'
        acc = plus + str(round(bundesland.acceleration * 100)) + '%'
        print('Änderung zum Vortag:', str(round(bundesland.diff_fract * 100)) + '%', '(' + acc + ')')
        print('Durchschnittliche Änderung pro Tag:', str(round(bundesland.av_diff_fract * 100)) + '%')
        print('Fälle pro 100.000 Einwohner:', bundesland.per_capita)
        #print('Durchseuchung:', str(round(bundesland.per_person * 100, 3)) + '%')
        print('Todesfälle:', bundesland.deaths)
        print('Differenz zum Vortag:', bundesland.death_diff)
        print('Änderung zum Vortag:', str(round(bundesland.death_diff_fract * 100)) + '%')
        plus = ''
        if bundesland.death_acc > 0:
            plus = '+'
        acc = plus + str(round(bundesland.death_acc * 100, 2)) + '%'
        print('Todesrate:', str(round(bundesland.death_rate * 100, 2)) + '%', '(' + acc + ')')
    print('\n------------------------------------------------------------------')

# uncomment next line to use with qpython for android
# requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = "TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-128-GCM-SHA256:TLS13-AES-256-GCM-SHA384:ECDHE:!COMPLEMENTOFDEFAULT"

shelfFile = shelve.open('data')
dictionary = shelfFile['dictionary']
shelfFile.close()

res = requests.get('https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html')

res.raise_for_status()

soup = bs4.BeautifulSoup(res.text, 'html.parser')

datestring = soup.select('div[class="text"] p')[1].getText()

rawdate = re.compile(r'\d(\d)?\.\d(\d)?\.\d\d\d\d').search(datestring).group()

datelist = rawdate.split('.')

date = datelist[2] + datelist[1].zfill(2) + datelist[0].zfill(2)

if not date in dictionary.keys():
    print('\nLade aktuelle Daten')

    table = soup.table

    table_rows = table.find_all('tr')

    datalist = []

    for tr in table_rows:
        td = tr.find_all('td')
        data = []
        for i in td:
            data.append(i.text)
        datalist.append(data)

    del datalist[:2]

    bundesland_list = []

    for data in datalist:
        bundesland_list.append(Bundesland(data))

    dictionary[date] = bundesland_list

    shelfFile = shelve.open('data')
    shelfFile['dictionary'] = dictionary
    shelfFile.close()

else:
    print('\nDaten sind aktuell!')

bundesland_list = dictionary[date]

today_date = datetime.datetime(int(datelist[2]), int(datelist[1]), int(datelist[0]))

yesterday_date = today_date - datetime.timedelta(days = 1)

yesterday_format = yesterday_date.strftime('%Y%m%d')

yesterday_list = dictionary[yesterday_format]

for i in range(len(bundesland_list)):
    sumlist = []
    for v in dictionary.values():
        v[i].calculate()
        sumlist.append(v[i])
    bundesland_list[i].average(sumlist)
    bundesland_list[i].compare(yesterday_list[i])  

while True:
    display()
    
    print('''
Nach Durchseuchung sortieren [a]
Nach Zunahme sortieren [d]
Nach Beschleunigung sortieren [b]
Nach durchschnittlicher Änderung sortieren [v]
Nach Todesrate sortieren [t]

Beenden [e]
''')

    ip = input()

    if ip == 'e':
        sys.exit()
    elif ip == 'a':
        bundesland_list = sorted(bundesland_list, key = lambda x: x.per_capita, reverse = True)
    elif ip == 'd':
        bundesland_list = sorted(bundesland_list, key = lambda x: x.diff_fract, reverse = True)
    elif ip == 't':
        bundesland_list = sorted(bundesland_list, key = lambda x: x.death_rate, reverse = True)
    elif ip == 'b':
        bundesland_list = sorted(bundesland_list, key = lambda x: x.acceleration, reverse = True)
    elif ip == 'v':
        bundesland_list = sorted(bundesland_list, key = lambda x: x.av_diff_fract, reverse = True)
